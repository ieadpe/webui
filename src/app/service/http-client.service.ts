import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

export class Account{
  constructor(
    public id:number,
    public nome:string,
    public login:string,
    public email:string,
    public senha:string,
    public role:string,
  ) {}
}

@Injectable({
  providedIn: 'root'
})
export class HttpClientService {

  constructor(
    private httpClient:HttpClient
  ) { }

  getAccounts(){
       console.log("test call");
       return this.httpClient.get<Account[]>('http://localhost:8762/conta?nome=&amp;login=&amp;email=');
  }
}
